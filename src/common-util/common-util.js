const numberRE = new RegExp(/^\d+/)
const isStringOfNumber = (str) => {
    return numberRE.test(str);
}

module.exports = {
    isStringOfNumber
}