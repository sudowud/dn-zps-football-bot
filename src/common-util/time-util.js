/**
 * 
 * @param {Date} _date 
 * @returns {String}
 */
 function toStringVietnamese(_date) {
	let dayInWeek = ["chủ nhật", "thứ hai", "thứ ba", "thứ tư", "thứ năm", "thứ sáu", "thứ bảy"];
	let _format = "%hour:%minute %dayInWeek (%dayInMonth/%month)";
	return _format.replace("%hour", _date.getHours() < 10 ? "0" + _date.getHours() : _date.getHours() )
				  .replace("%minute", _date.getMinutes() < 10 ? "0" + _date.getMinutes() : _date.getMinutes() )
				  .replace("%dayInWeek", dayInWeek[_date.getDay()])
				  .replace("%dayInMonth", _date.getDate())
				  .replace("%month", _date.getMonth() + 1);

}

const DAY_IN_WEEK = {
	SUNDAY: 0,
	MONDAY: 1,
	TUESDAY: 2,
	WEDNESDAY: 3,
	THURSDAY: 4,
	FRIDAY: 5,
	SATURDAY: 6
}

module.exports = {
	toStringVietnamese,
	DAY_IN_WEEK
}

