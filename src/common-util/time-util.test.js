const time_util = require("./time-util");

test('toStringVietnamese', () => {
	expect(time_util.toStringVietnamese(new Date("2022-3-13 12:00:00"))).toBe("12:00 chủ nhật (13/3)");
})

