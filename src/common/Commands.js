const {MEMBER_STATUS} = require("../modules/members/member-const");
const {VOTE_RESULTS} = require("../modules/matches/match-const");


const COMMANDS = {
	help: "help",
	check: "check",
	check_with_info: "check_with_info",
	set_group_football: "set_group_football",
	set_group_notification: "set_group_notification",
	remove_group_notification: "remove_group_notification",
	get_all_member: "get_all_member",
	register_member: "register_member",
	set_role: "set_role",
	set_member_status: "set_member_status",
	new_match: "new_match",
	stop_polling: "stop_polling",
	cancel_match: "cancel_match",
	delete_match: "delete_match",
	get_match: "get_match",
	force_vote: "force_vote",
	remind_vote: "remind_vote",
	late: "late",
	give_up: "give_up",
	get_all_statistic: "get_all_statistic"
}

const DESCRIPTION = {};

for (let command in COMMANDS) {
	let description = "Chưa được thêm mô tả";
	switch (command) {
		case COMMANDS.help:
			description = "Bảng hỗ trợ.";
			break;
		case COMMANDS.set_group_football:
			description = `Set group hiện tại thành group chính của bot.`;
			break;
		case COMMANDS.set_group_notification:
			description = `Set group hiện tại thành group nhận thông báo lỗi.`;
			break;
		case COMMANDS.remove_group_notification:
			description = `Bỏ group thông báo lỗi`;
			break;
		case COMMANDS.get_all_member:
			description = `Lấy tất cả các thông tin user. params: [domain] [username] [role]`;
			break;
		case COMMANDS.register_member:
			description = `Đăng ký domain cho username, params: domain username.`;
			break;
		case COMMANDS.set_member_status:
			description = `Set status cho member, status: ${Object.keys(MEMBER_STATUS)}, params: domain username.`;
			break;
		case COMMANDS.set_role:
			description = `Set quyền cho user, role là "admin" hoặc "normal", params: username role.`;
			break;
		case COMMANDS.new_match:
			description = `Tạo poll trận đấu mới. Tham số: yyyy-mm-dd hh:mm:ss tên sân.`
			break;
		case COMMANDS.stop_polling:
			description = `Dừng vote trận đấu. Tham số: [matchId].`;
			break;
		case COMMANDS.cancel_match:
			description = `Huỷ trận đấu, nhưng không xoá dữ liệu vote. Tham số: [matchId].`;
			break;
		case COMMANDS.delete_match:
			description = `Xoá trận đấu. Tham số: [matchId].`;
			break;
		case COMMANDS.get_match:
			description = `JSON trận đấu. Tham số: [matchId].`;
			break;
		case COMMANDS.force_vote:
			description = `Admin force chỉnh kết quả vote user, vote: ${Object.keys(VOTE_RESULTS)}. Tham số: matchId username vote [reason]`;
			break;
		case COMMANDS.remind_vote:
			description = `Nhắc nhở vote. Tham số : [matchId].`;
			break;
		case COMMANDS.late:
			description = `Đánh trễ user. Tham số: username1 [username2].`;
			break;
		case COMMANDS.give_up:
			description = `Đánh bỏ cuộc. Tham số: username1 [username2].`;
			break;
		case COMMANDS.check:
			description = `Hiển thị gọn thông tin user trong tháng. Tham số: [username] [mm] [yyy].`;
			break;
		case COMMANDS.check_with_info:
			description = `Hiển thị thông tin từng buổi của user trong tháng. Tham số: [username] [mm] [yyyy].`;
			break;
		case COMMANDS.get_all_statistic:
			description = `Lấy thông tin thống kê. Tham số: [mm] [yyyy].`;
			break;
	}
	DESCRIPTION[command] = description;
}

function formatBotCommand() {
	let commands = []

	for (let command in DESCRIPTION) {
		commands.push({
			command,
			description: DESCRIPTION[command]
		})
	}
	return commands;
}

function getHelps() {
	let help = `* Quy ước: 
				 - matchId: yymmdd
				 - Các params bọc [] thì có thể không truyền vào, sẽ lấy giá trị mặc đinh phù hợp.		
	`
	help += "\n* Các lệnh: ";
	for (let command in COMMANDS) {
		help += `\n/${command} - ${DESCRIPTION[command]}`
	}
	return help;
}

module.exports = {
	COMMANDS,
	formatBotCommand,
	getHelps
}