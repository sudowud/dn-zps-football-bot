const { startBot } = require("./services/bot-service");
const { logger } = require("./services/log-service");
const connectDB = require("./database/connect");


const start = async () => {
	try {
		await connectDB().then(() => console.log("connected to db successfully"));
		startBot()
		require("./services/schedule-service").runSchedule();
		logger.info("Server started");
	}
	catch(e) {
		logger.error("Không thể khởi động bot")
		logger.info(e);
	}
}

start();