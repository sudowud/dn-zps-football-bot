const {getMemberByUsername} = require("../modules/members/member-cached-repository");


async function determineRole(ctx, next) {
	if (ctx.from?.username === "locvb" || ctx.from?.username === "zps_football_bot") {
		ctx.state.role = "superadmin";
	}
	else {
		if (ctx.from?.username) {
			ctx.from.username = ctx.from.username.toLowerCase();
			let member = await getMemberByUsername(ctx.from?.username);
			if (member) {
				ctx.state.role = member.role;
			}
			else {
				// let ignoredMember = await member_service.getIgnoreUsernames();
				// if (!ignoredMember.includes(ctx.from.username)) {

				// 	ctx.reply(ctx.from?.username + " chưa được đăng ký, hãy liên hệ gấp với admin");
				// }
			}
		}
		if (!ctx.state.role) ctx.state.role = "normal";
	}

	next();
}

async function isAdmin(ctx, next) {
	if (ctx.state.role.includes("admin")) {
		next();
	}
	else {
		ctx.reply("Cần quyền admin để thực hiện command này!!!");
	}
}

async function isSuperAdmin(ctx, next) {
	if (ctx.state.role.includes("superadmin")) {
		next();
	}
	else {
		ctx.reply("Cần quyền superadmin để thực hiện command này!!!");
	}
}

module.exports = {
	determineRole,
	isAdmin,
	isSuperAdmin
}
