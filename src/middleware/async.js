const {logger} = require("../services/log-service");
const {sendToGroupNotification} = require("../services/noti-service");
const asyncWrapper = (fn) => {
    return async(ctx) => {
        try {
            await fn(ctx);
        }
        catch(error) {
            logger.error(error);
            try {
                await sendToGroupNotification(error);
                ctx.reply && ctx.reply("Đã có lỗi xảy ra!!!");
            }
            catch (e) {
            }
        }
    }
}

module.exports = asyncWrapper