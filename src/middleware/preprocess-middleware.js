async function removeAmpersandFromNames(ctx, next) {
	if (ctx.message?.text) {
		ctx.message.text = ctx.message.text.replace(/\@/g, "");
	}

	// if (ctx.chat.type != "group") {
	// 	ctx.reply("Không được chat riêng với bot, hãy chat vào group!!!");
	// 	return;
	// }

	next();
}

module.exports = {
	removeAmpersandFromNames
}