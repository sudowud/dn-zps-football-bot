const {processNewMatch, processStopPoll, processDeleteMatch, processVoteMatchPoll, processNewNextMatchByDefault,
	processRemindVote, processMarkLate, processMarkGiveup, getStatisticMatches, processForceVote, processCheckUser,
	processCheckWithInfoUser, processCancelMatch, processGetMatch
} = require("./match-service");
const {formatName} = require("../members/member-util");
const {getNextMatch, getMatchId} = require("./match-util");
const {isStringOfNumber} = require("../../common-util/common-util");

async function commandNewMatch(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	if (splited.length == 1) {
		return await processNewNextMatchByDefault();
	}

	if (splited.length < 3) {
		return "Cú pháp lỗi. Kiểm tra theo mẫu: /new_match yyyy-mm-dd hh:mm:ss tên sân bóng";
	}
	const regex = new RegExp(/\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}:\d{1,2}/);

	let dateString = splited[1] + " " + splited[2];
	if (!regex.test(dateString)) {
		return "Thời gian truyền vào lỗi";
	}
	let date = new Date(dateString);

	if (date < new Date()) {
		return "Thời gian truyền vào phải lớn hơn hiện tại";
	}

	let pitch;
	if (splited.length >= 4) { pitch = "sân " + splited.slice(3).join(" ") }
	else {
		pitch = "sân chưa xác định";
	}
	return await processNewMatch(date, pitch);
}

async function commandStopPoll(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");

	if (splited.length != 2) {
		return "Cú pháp lỗi. Kiểm tra lại mẫu";
	}

	let matchId = splited[1];
	if (!isStringOfNumber(matchId)) return "Match id là số";
	return await processStopPoll(matchId);
}

async function commandCancelMatch(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");

	if (splited.length != 2) {
		return "Cú pháp lỗi. Kiểm tra lại mẫu";
	}

	let matchId = splited[1];
	if (!isStringOfNumber(matchId)) return "Match id là số";
	return await processCancelMatch(matchId);
}

async function commandGetMatch(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");

	if (splited.length != 2) {
		return "Cú pháp lỗi. Kiểm tra lại mẫu";
	}

	let matchId = splited[1];
	if (!isStringOfNumber(matchId)) return "Match id là số";
	return await processGetMatch(matchId);
}


async function commandDeleteMatch(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	if (splited.length != 2) {
		return "Cú pháp lỗi. Kiểm tra theo mẫu: /cancel_match matchId";
	}
	let matchId = splited[1];
	if (!isStringOfNumber(matchId)) return "Match id là số";
	return await processDeleteMatch(matchId);
}

async function commandForceVote(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	splited.shift();

	let matchId = splited[0];
	let username = splited[1];
	let answer = splited[2];

	if (!isStringOfNumber(matchId)) return "Match id là số";

	let reason = undefined;
	if (splited.length > 3) {
		reason = splited.slice(3).join(" ").replaceAll("\|", "");
	}

	return await processForceVote(matchId, username, answer, reason);
}

async function commandVoteMatchPoll(pollingId, username, answer) {
	username = formatName(username);

	return processVoteMatchPoll(pollingId, username, answer);
}

async function commandRemindVote(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");

	if (splited.length > 2) {
		return "Cú pháp lỗi. Kiểm tra lại mẫu";
	}
	let matchId = splited[1] ? splited[1] : getMatchId(getNextMatch(new Date()));
	if (!isStringOfNumber(matchId)) return "Match id là số";
	return await processRemindVote(matchId);
}

async function commandLate(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	splited.shift();
	if (splited.length == 0) {
		return "Cần truyền ít nhất một username";
	}

	let matchId;
	const regex = new RegExp(/^\d+/);
	if (regex.test(splited[0])) {
		matchId = splited[0];
		splited.shift();
	}
	else {
		matchId = getMatchId(new Date());
	}

	return processMarkLate(matchId, splited);
}

async function commandGiveUp(content) {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	splited.shift();
	if (splited.length == 0) {
		return "Cần truyền ít nhất một username";
	}

	let matchId;
	const regex = new RegExp(/^\d+/);
	if (regex.test(splited[0])) {
		matchId = splited[0];
		splited.shift();
	}
	else {
		matchId = getMatchId(new Date());
	}

	return processMarkGiveup(matchId, splited);
}

async function commandCheckUser(sendingUsername, content) {
	let username = sendingUsername;
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	splited.shift();

	const regex = new RegExp(/.*[a-z]+.*/);
	if (splited.length > 0 && regex.test(formatName(splited[0]))) {
		username = formatName(splited[0]);
		splited.shift();
	}

	let year = new Date().getFullYear();
	let month = new Date().getMonth() + 1;
	if (splited[0]) {
		month = Number(splited[0]);
	}

	if (splited[1]) {
		year = Number(splited[1]);
	}

	return await processCheckUser(username, year, month);
}

async function commandCheckWithInfoUser(sendingUsername, content) {
	let username = sendingUsername;
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	splited.shift();

	const regex = new RegExp(/.*[a-z]+.*/);
	if (splited.length > 0 && regex.test(formatName(splited[0]))) {
		username = formatName(splited[0]);
		splited.shift();
	}

	let year = new Date().getFullYear();
	let month = new Date().getMonth() + 1;
	if (splited[0]) {
		month = Number(splited[0]);
	}

	if (splited[1]) {
		year = Number(splited[1]);
	}

	return await processCheckWithInfoUser(username, year, month);
}

async function commandStatisticAll(content) {
	let year = new Date().getFullYear();
	let month = new Date().getMonth() + 1;
	let splited = content.split(" ");
	if (splited[1]) {
		month = Number(splited[1]);
	}

	if (splited[2]) {
		year = Number(splited[2]);
	}

	return await getStatisticMatches(year, month);
}



module.exports = {
    commandNewMatch,
	commandStopPoll,
	commandCancelMatch,
	commandDeleteMatch: commandDeleteMatch,
	commandVoteMatchPoll,
	commandRemindVote,
	commandLate,
	commandGiveUp,
	commandCheckUser,
	commandCheckWithInfoUser,
	commandStatisticAll,
	commandForceVote,
	commandGetMatch
}