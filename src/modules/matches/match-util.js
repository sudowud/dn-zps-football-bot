/**
 *
 * @param {Date} _date
 * @returns {Date} next match time
 */
const { VOTE_RESULTS } = require("./match-const");
const _ = require("lodash");
const { DAY_IN_WEEK } = require("../../common-util/time-util");

function getNextMatch(_date) {
	res.setDate(_date.getDate() + ((7 - _date.getDay()) % 7 + 4) % 7);
	return res;
}

/**
 * @param {Date} playDate 
 * @returns {String} match id
 */
function getMatchId(playDate) {
	let fillZero = (num) => num <= 9 ? "0" + num : num;
	return Number("" + playDate.getFullYear() % 100 + fillZero(playDate.getMonth() + 1) + fillZero(playDate.getDate()));
}

const convertVoteToWord = function (vote) {
	let result = _.invert(VOTE_RESULTS)[vote];
	return result || VOTE_RESULTS.NO_VOTE;
}

module.exports = {
	getNextMatch,
	getMatchId,
	convertVoteToWord
}