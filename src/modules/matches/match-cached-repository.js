const repo = require("./match-repository");
let cachedMatches = [];

const getMatchById = async (matchId) => {
    let match = cachedMatches.find((match) => match.matchId == matchId);
    if (match) return match;

    let matchInDB = await repo.getMatch({matchId});
    if (matchInDB) cachedMatches.push(matchInDB);
    return matchInDB;
}

const getMatchByPollingId = async (pollingId) => {
    let match = cachedMatches.find((match) => match.pollingId == pollingId);
    if (match) return match;

    let matchInDB = await repo.getMatch({pollingId});
    if (matchInDB) cachedMatches.push(matchInDB);
    return matchInDB;
}

const deleteMatchById = async (matchId) => {
    let match = await repo.deleteMatchById(matchId);
    let index = cachedMatches.findIndex((match) => match.matchId == matchId);
    if (index >=0) {
        cachedMatches.splice(index, 1);
    }

    return match;
}

const updateMatch = async (match) => {
    let savedMatch = await repo.updateMatch(match);
    let index = cachedMatches.findIndex((cacheMatch) => cacheMatch.matchId == match.matchId);
    if (index >=0) {
        cachedMatches[index] = savedMatch;
    } else {
        if (cachedMatches.length > 20) {
            cachedMatches.splice(20, cachedMatches.length - 20);
        }

        cachedMatches.unshift(savedMatch);
    }

    return savedMatch;
}

module.exports = {
    getMatchById,
    getMatchByPollingId,
    updateMatch,
    deleteMatchById
}