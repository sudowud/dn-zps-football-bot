const VOTE_RESULTS = {
    NO_VOTE: -1,
    YES: 0,
    NO: 1,
    LONG_OFF: 2,
    LEGAL_OFF: 3
}

module.exports = {
    VOTE_RESULTS
}