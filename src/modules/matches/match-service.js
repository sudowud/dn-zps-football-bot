const { getNextMatch, getMatchId } = require("./match-util");
const { getMatchesToday, getMatches } = require("./match-repository");
const { logger } = require("../../services/log-service");
const { bot } = require("../../common/bot-tele");
const { getGroupIdOfFootBall } = require("../misc/misc-repository");
const { formatName } = require("../members/member-util");
const { toStringVietnamese } = require("../../common-util/time-util");
const { VOTE_RESULTS } = require("./match-const");
const { MEMBER_STATUS } = require("../members/member-const");
const { sendToGroupNotification } = require("../../services/noti-service");
const { getAllMembers, getMemberByUsername } = require("../members/member-cached-repository");
const cachedRepo = require("./match-cached-repository");


const processNewMatch = async (playDate, pitch) => {
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) {
		return "Chưa set group id";
	}
	let matchId = getMatchId(playDate);
	let match = await cachedRepo.getMatchById(matchId);
	if (match) {
		return "Đã có trận đấu trong ngày rồi: " + toStringVietnamese(playDate);
	}
	logger.trace("processNewMatch sending poll")
	let res = await bot.telegram.sendPoll(groupId, `[${matchId}] Điểm danh ${toStringVietnamese(playDate)} ${pitch}`, ["Có", "Không"],
		{ allows_multiple_answers: false, is_anonymous: false }
	);
	logger.trace(`processNewMatch receiving poll ${JSON.stringify(res)}`)

	if (!res?.message_id) {
		await sendToGroupNotification("Không thể tạo trận do response tạo poll thiếu message_id");
		return;
	}

	if (!res?.poll?.id) {
		await sendToGroupNotification("Không thể tạo trận do response tạo poll thiếu poll id");
		return;
	}

	if (res) {
		await cachedRepo.updateMatch({
			matchId,
			message_id: res.message_id,
			playDate,
			pollingId: res.poll?.id,
			vote: {},
			late: [],
			giveup: [],
			isClosed: false
		}
		);

		setTimeout(async () => {
			try {
				logger.trace("processNewMatch send pin")
				await bot.telegram.pinChatMessage(groupId, res.message_id);
			} catch (error) {

			}
		}, 1000)
		return "";
	}
	else {
		logger.warn("can't create new match", "res from send poll", res);
	}
	return "Tạo trận thất bại";
}

const processNewNextMatchByDefault = async () => {
	let playDate = getNextMatch(new Date());
	let pitch = "sân Harmony";
	return await processNewMatch(playDate, pitch);
}

const processStopPoll = async (matchId) => {
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) return "stop poll thất bại, chưa set group id";

	let match = await cachedRepo.getMatchById(matchId);
	if (!match) return "Trận đấu không tồn tại";

	let members = await getAllMembers();
	for (let member of members) {
		if (member.status != MEMBER_STATUS.NORMAL) {
			match.vote[member.domain] = {
				status: VOTE_RESULTS.LONG_OFF
			};
		}
	}

	match.isClosed = true;
	await cachedRepo.updateMatch(match);
	await bot.telegram.stopPoll(groupId, Number(match.message_id))
	await bot.telegram.sendMessage(groupId, "Đã dừng vote này!!!", { reply_to_message_id: match.message_id });
}

const processCancelMatch = async (matchId) => {
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) return "cancel poll thất bại, chưa set group id";

	let match = await cachedRepo.getMatchById(matchId);
	if (!match) return "Trận đấu không tồn tại";

	let members = await getAllMembers();
	for (let member of members) {
		if (member.status != MEMBER_STATUS.NORMAL) {
			match.vote[member.domain] = {
				status: VOTE_RESULTS.LONG_OFF
			};
		}
	}

	match.isClosed = true;
	match.isCancelled = true;

	await cachedRepo.updateMatch(match);
	await bot.telegram.stopPoll(groupId, Number(match.message_id))
	await bot.telegram.sendMessage(groupId, "Đã cancel trận đấu này!!!", { reply_to_message_id: match.message_id });
}

const processGetMatch = async (matchId) => {
	let match = await cachedRepo.getMatchById(matchId);
	if (!match) return "Trận đấu không tồn tại";

	return JSON.stringify(match);
}

const processDeleteMatch = async (matchId) => {
	let match = await cachedRepo.getMatchById(matchId);
	if (!match) {
		return "Không tồn tại match id: " + matchId;
	}
	let deleteResult = await cachedRepo.deleteMatchById(matchId);
	if (deleteResult.deletedCount == 0) {
		return "Không thể xoá trận khỏi db " + matchId;
	}

	let groupId = await getGroupIdOfFootBall();
	if (groupId) {
		bot.telegram.deleteMessage(groupId, match.message_id);
	}
	logger.info("deletePolling", match.message_id);
	return "Huỷ trận đấu " + matchId + " thành công!!!";
}

async function processForceVote(matchId, username, stringAnswer, reason) {
	let match = await cachedRepo.getMatchById(matchId);
	if (!match) {
		return "Không tồn tại match id: " + matchId;
	}

	let member = await getMemberByUsername(username);
	if (!member) {
		return `Username "${username}" chưa được đăng ký`;
	}

	stringAnswer = stringAnswer.toUpperCase();
	if (!VOTE_RESULTS.hasOwnProperty(stringAnswer)) return `Answer "${stringAnswer}" không thuộc tập cho phép: ` + Object.keys(VOTE_RESULTS);

	match.vote[member.domain] = {
		status: VOTE_RESULTS[stringAnswer],
		info: reason || ""
	};
	await cachedRepo.updateMatch(match);

	return `Update thành công`;
}

async function processVoteMatchPoll(pollingId, username, answer) {
	let match = await cachedRepo.getMatchByPollingId(pollingId);
	if (!match) {
		throw "Polling chưa được lưu vào DB";
	}

	if (match.isClosed) {
		throw `Đã đóng vote rồi @${username}, lần vote vừa rồi không được công nhận`;
	}

	let member = await getMemberByUsername(username);
	if (!member) {
		throw `processVoteMatchPoll username "${username}" chưa được đăng ký`;
	}

	if (member.status != MEMBER_STATUS.NORMAL) {
		let message = `@${member.username} đang bị ${member.status} thì không vote nha.`;
		let groupNoti = await getGroupIdOfFootBall();
		if (groupNoti) {
			await bot.telegram.sendMessage(groupNoti, message);
		}
		return;
	}

	if (!match.vote) match.vote = {};

	if (match.vote[member.domain]?.status === VOTE_RESULTS.LEGAL_OFF) {
		let message = `Trận [${match.matchId}], @${member.username} đã được đăng ký legal_off với lý do ${match.vote[member.domain].info || "chưa rõ"}. Liên hệ admin để set lại nếu có sự thay đổi, lần vote vừa rồi không được tính`;
		let groupNoti = await getGroupIdOfFootBall();
		if (groupNoti) {
			await bot.telegram.sendMessage(groupNoti, message);
		}
		return;
	}

	if (answer === undefined) answer = VOTE_RESULTS.NO_VOTE

	match.vote[member.domain] = {
		status: answer
	};
	await cachedRepo.updateMatch(match);
	await logger.info("onPollAnswer", match.matchId, member.domain, answer);
}

const processRemindVote = async (matchId) => {
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) {
		return "group id chưa được set";
	}

	let match = await cachedRepo.getMatchById(matchId);
	if (!match || match.isClosed) {
		return "Trận đấu không tồn tại, hoặc đã đóng vote: " + matchId;

	}

	let members = await getAllMembers();
	let unvoteMembers = []

	for (let member of members) {
		if (member.status != MEMBER_STATUS.NORMAL) continue;

		let userVote = match.vote?.[member.domain] || {};
		if (userVote.status === undefined || userVote.status == VOTE_RESULTS.NO_VOTE) {
			unvoteMembers.push(member.username);
		}
	}
	if (unvoteMembers.length > 0) {
		let msg = "Gần hết thời gian điểm danh rồi, vote nhanh nào";
		for (let username of unvoteMembers) {
			msg += " @" + username;
		}
		await bot.telegram.sendMessage(groupId, msg, { reply_to_message_id: match.message_id });
	}
}

const processMarkLate = async (matchId, listUsername) => {
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) {
		return "group id chưa được set";
	}

	let match = await cachedRepo.getMatchById(matchId);
	if (!match) {
		return "Không tồn tại trận đấu id: " + matchId;
	}

	let arr = [];
	for (let username of listUsername) {
		username = formatName(username);
		let member = await getMemberByUsername(username);
		if (member) {
			if (match.late && !match.late.includes(member.domain)) {
				match.late.push(member.domain);
			}
			arr.push(username);
		}
		else {
			try {
				bot.telegram.sendMessage(groupId, `Username "${username}" chưa được đăng ký`);
			}
			catch (e) {

			}
		}
	}
	await cachedRepo.updateMatch(match);
	let msg = "";
	for (let username of arr) {
		msg += " @" + username;
	}
	if (msg) {
		return "Đã đánh trễ " + msg;
	}
}

const processMarkGiveup = async (matchId, listUsername) => {
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) {
		return "group id chưa được set";
	}

	let match = await cachedRepo.getMatchById(matchId);
	if (!match) {
		return "Không tồn tại trận đấu id: " + matchId;
	}
	let arr = [];
	for (let username of listUsername) {
		username = formatName(username);
		let member = await getMemberByUsername(username);
		if (member) {
			if (match.giveup && !match.giveup.includes(member.domain)) {
				match.giveup.push(member.domain);
			}
			arr.push(username);
		}
		else {
			try {
				bot.telegram.sendMessage(groupId, `Username "${username}" chưa được đăng ký`);
			}
			catch (e) {

			}
		}
	}
	await cachedRepo.updateMatch(match);
	let msg = "";
	for (let username of arr) {
		msg += " @" + username;
	}
	if (msg) {
		return "Đã đánh vắng " + msg;
	}
}

const processUnpinCurrentDayMatches = async () => {
	let matches = await getMatchesToday();
	let groupId = await getGroupIdOfFootBall();
	if (!groupId) return;
	if (matches) {
		for (let match of matches) {
			try {
				let resUnpin = await bot.telegram.unpinChatMessage(groupId, Number(match.message_id))
			}
			catch (e) {

			}
		}
	}
}

async function processCheckUser(username, year, month) {
	let member = await getMemberByUsername(username);
	if (!member) return `"${username}" chưa được đăng ký username`;

	let domain = member.domain;

	if (!year) year = new Date().getFullYear();
	if (!month) month = new Date().getMonth() + 1;

	let filter = {
		"playDate": {
			$gte: new Date(new Date(year, month - 1, 1).setHours(0, 0, 0)),
			$lt: new Date(new Date(year, month, 0).setHours(23, 59, 59))
		},
		"isClosed": {
			$eq: true
		}
	};

	let matches = await getMatches(filter);

	let data = {
		absent: 0,
		late: 0,
		giveup: 0
	}
	for (let match of matches) {
		let vote = match.vote || {};
		let giveup = match.giveup || [];
		let late = match.late || [];


		let userVote = vote[domain] || {};
		let voteResult = userVote.status;
		if (voteResult === undefined || voteResult === VOTE_RESULTS.NO_VOTE) {
			data["giveup"]++;
		}
		if (voteResult === VOTE_RESULTS.YES) {
			if (late.includes(domain)) {
				data["late"]++;
			}
			if (giveup.includes(domain)) {
				data["giveup"]++;
			}
		}
		if (voteResult === VOTE_RESULTS.NO) {
			data["absent"]++;
		}
	}
	return `${username} - Tháng ${month} năm ${year}:
		- Vắng: ${data["absent"]}
		- Trễ: ${data["late"]}
		- Bỏ cuộc: ${data["giveup"]}
	`;
}

async function processCheckWithInfoUser(username, year, month) {
	let member = await getMemberByUsername(username);
	if (!member) return `"${username}" chưa được đăng ký username`;
	let domain = member.domain;

	if (!year) year = new Date().getFullYear();
	if (!month) month = new Date().getMonth() + 1;

	let filter = {
		"playDate": {
			$gte: new Date(new Date(year, month - 1, 1).setHours(0, 0, 0)),
			$lt: new Date(new Date(year, month, 0).setHours(23, 59, 59))
		},
		"isClosed": {
			$eq: true
		}
	};

	let matches = await getMatches(filter);

	let info = "";

	for (let match of matches) {
		let vote = match.vote || {};
		let giveup = match.giveup || [];
		let late = match.late || [];

		let tick = "OK";
		let userVote = vote[domain] || {};
		let voteStatus = userVote.status;
		if (voteStatus === undefined || voteStatus === VOTE_RESULTS.NO_VOTE) {
			tick = "no vote";
		}
		if (voteStatus === VOTE_RESULTS.LONG_OFF) {
			tick = "long_off";
		}
		if (voteStatus === VOTE_RESULTS.LEGAL_OFF) {
			tick = `legal_off(${userVote.info || "Không điền info"})`
		}

		if (voteStatus === VOTE_RESULTS.YES) {
			if (late.includes(domain)) {
				tick = "late";
			}
			if (giveup.includes(domain)) {
				tick = "give up";
			}
		}
		if (voteStatus === VOTE_RESULTS.NO) {
			tick = "absent";
		}

		if (match.isCancelled) tick += ", nhưng trận đấu này bị cancel";

		info += `\n- ${toStringVietnamese(match.playDate)}: ${tick}`
	}
	return `${username} - Tháng ${month} năm ${year}:${info}`;
}


async function getStatisticMatches(year, month) {
	let filter = {
		"playDate": {
			$gte: new Date(new Date(year, month - 1, 1).setHours(0, 0, 0)),
			$lt: new Date(new Date(year, month, 0).setHours(23, 59, 59))
		},
		"isClosed": {
			$eq: true
		}
	};
	let matches = await getMatches(filter);
	let allMembers = await getAllMembers();
	let header = "domain|absent|late|giveup";
	let rows = {};

	for (let match of matches) {
		header += "|" + toStringVietnamese(match.playDate);
		let vote = match.vote || {};

		let giveup = match.giveup || [];
		let late = match.late || [];
		for (let member of allMembers) {
			let domain = member.domain;
			if (!rows[domain]) {
				rows[domain] = {
					history: "",
					absent: 0,
					late: 0,
					giveup: 0
				};
			}

			let tick = "OK";
			let userVote = vote[domain] || {};
			let voteStatus = userVote.status;
			if (voteStatus === undefined || voteStatus === VOTE_RESULTS.NO_VOTE) {
				tick = "no vote";
				rows[domain]["giveup"]++;
			}
			if (voteStatus === VOTE_RESULTS.LONG_OFF) {
				tick = "long_off";
			}
			if (voteStatus === VOTE_RESULTS.LEGAL_OFF) {
				tick = `legal_off(${userVote.info || "Không điền info"})`
			}

			if (voteStatus === VOTE_RESULTS.YES) {
				if (late.includes(domain)) {
					tick = "late";
					rows[domain]["late"]++;
				}
				if (giveup.includes(domain)) {
					tick = "give up";
					rows[domain]["giveup"]++;
				}
			}
			if (voteStatus === VOTE_RESULTS.NO) {
				tick = "absent";
				rows[domain]["absent"]++;
			}

			rows[domain]["history"] += "|" + tick;
		}
	}
	let table = header;
	for (let mem in rows) {
		let row = "\n" + mem;
		row += "|" + rows[mem]["absent"];
		row += "|" + rows[mem]["late"];
		row += "|" + rows[mem]["giveup"];
		row += rows[mem]["history"];
		table += row;
	}
	return table;
}

module.exports = {
	processNewMatch,
	processNewNextMatchByDefault,
	processStopPoll,
	processCancelMatch,
	processDeleteMatch,
	processGetMatch,
	processForceVote,
	processVoteMatchPoll,
	processMarkLate,
	processRemindVote,
	processMarkGiveup,
	processCheckUser,
	processCheckWithInfoUser,
	processUnpinCurrentDayMatches,
	getStatisticMatches
}