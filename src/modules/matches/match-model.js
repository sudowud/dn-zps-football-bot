const mongoose = require('mongoose')
const Match = new mongoose.Schema({
    matchId: Number,
    message_id: Number,
    pollingId: Number,
    playDate: Date,
    vote: Object,
    late: Array,
    giveup: Array,
    isClosed: {
        type: Boolean,
        default: false
    },
    isCancelled: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('Match', Match);