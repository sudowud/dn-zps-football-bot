const Match = require("./match-model")


const getMatch = async(filter) => {
    return await Match.findOne(filter).exec();
}

const getMatchById = async (matchId) => {
    return await getMatch({matchId: matchId});
}

const getMatchByPollingId = async (pollingId) => {
    return await getMatch({pollingId});
}

const getMatches = async(filter) => {
	return await Match.find(filter).exec();
}

const getMatchesToday = async() => {
    return await getMatches({
        "playDate": {
            $gte: new Date(new Date().setHours(0, 0, 0)),
            $lt: new Date(new Date().setHours(23, 59, 59))
        }
    })
}

const getAllMatches = async() => {
    return await Match.find({}).exec();
}

const updateMatch = async(match) => {
    return Match.findOneAndUpdate(
        {
            matchId: match.matchId
        },
        match,
        { 
            new: true,
            upsert: true,
            runValidators: true
        }
    )
}

const deleteMatchById = async (matchId) => {
    return Match.deleteOne({matchId});
}

module.exports = {
    getMatch,
	getMatches,
    getMatchesToday,
    getAllMatches,
	updateMatch,
    deleteMatchById
}