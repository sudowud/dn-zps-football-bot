const {bot} = require("../../common/bot-tele");
const {COMMANDS} = require("../../common/Commands");
const asyncWrapper = require("../../middleware/async");
const authorMiddle = require("../../middleware/author-middleware");
const matchController = require("./match-controller");
const {logger} = require("../../services/log-service");
const {commandRemindVote, commandLate, commandGiveUp, commandStatisticAll, commandCheckUser, commandCheckWithInfoUser} = require("./match-controller");

const addMatchHandler = function () {
    bot.command(COMMANDS.new_match, authorMiddle.isAdmin,asyncWrapper (async (ctx) => {
        let response = await matchController.commandNewMatch(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.new_match}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.stop_polling, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await matchController.commandStopPoll(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.stop_polling}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.cancel_match, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await matchController.commandCancelMatch(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.cancel_match}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.delete_match, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await matchController.commandDeleteMatch(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.delete_match}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.get_match, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await matchController.commandGetMatch(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.get_match}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.force_vote, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await matchController.commandForceVote(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.force_vote}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.on("poll_answer", asyncWrapper(async (ctx) => {
        logger.info(`[poll_answer]`, ctx.update.poll_answer.user.username, ctx.update.poll_answer.poll_id, ctx.update.poll_answer.option_ids[0]);
        await matchController.commandVoteMatchPoll(ctx.update.poll_answer.poll_id, ctx.update.poll_answer.user.username, ctx.update.poll_answer.option_ids[0]);
    }));

    bot.command(COMMANDS.remind_vote, asyncWrapper(async (ctx) => {
        let response = await commandRemindVote(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.remind_vote}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.late, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await commandLate(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.late}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));


    bot.command(COMMANDS.give_up, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
        let response = await commandGiveUp(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.give_up}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.check, asyncWrapper(async (ctx) => {
        let response = await commandCheckUser(ctx.from?.username, ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.check}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.check_with_info, asyncWrapper(async (ctx) => {
        let response = await commandCheckWithInfoUser(ctx.from?.username, ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.check_with_info}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));

    bot.command(COMMANDS.get_all_statistic, asyncWrapper(async (ctx) => {
        let response = await commandStatisticAll(ctx.message.text);
        response && ctx.reply(response);
        logger.info(`[${COMMANDS.get_all_statistic}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
    }));
}

module.exports = {
    addMatchHandler
}