const Member = require("./member-model")

const getMember = async(filter) => {
    return await Member.findOne(filter).exec();
}

const getAllMembers = async() => {
	return await Member.find({}).exec();
}

const updateMember = async(member) => {
    return await Member.findOneAndUpdate(
        {
            domain: member.domain
        },
        member,
        { 
            new: true,
            upsert: true,
            runValidators: true
        }
    )
}

module.exports = {
    getMember,
    getAllMembers,
    updateMember
}