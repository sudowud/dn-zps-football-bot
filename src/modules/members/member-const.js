const MEMBER_STATUS = {
    NORMAL: "NORMAL",
    OFF: "OFF",
    IGNORED: "IGNORED"
}

module.exports = {
    MEMBER_STATUS: MEMBER_STATUS
}