const mongoose = require('mongoose')
const {MEMBER_STATUS} = require("./member-const");
const Member = new mongoose.Schema({
    domain: {
        type: String,
        required: [true, "must be provied domain"],
        trim: true,
        unique: true
    },
    username: {
        type: String,
        required: [true, "must be provied username"],
        trim: true,
        unique: true
    },
    role: {
        type: String,
        trim: true,
        enum: {
            values: ["normal", "admin"],
            message: 'Role "{VALUE}" is not supported'
        },
        default: "normal"
    },
    status: {
        type: String,
        trim: true,
        enum: {
            values: Object.keys(MEMBER_STATUS),
            message: 'Status "{VALUE}" is not supported'
        },
        default: MEMBER_STATUS.NORMAL
    }
})

module.exports = mongoose.model('Member', Member);