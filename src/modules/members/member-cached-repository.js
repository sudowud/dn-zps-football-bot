const repo = require("./member-repository");
let cachedMembers = repo.getAllMembers() || [];

setTimeout(async () => {
    cachedMembers = await repo.getAllMembers();
}, 100);

const getMemberByUsername = async (username) => {
    let member = cachedMembers.find((member) => member.username == username);
    if (member) return member;

    let memberInDB = await repo.getMember({username});
    if (memberInDB) cachedMembers.push(memberInDB);
    return memberInDB;
}

const getMemberByDomain = async (domain) => {
    let member = cachedMembers.find((member) => member.domain == domain);
    if (member) return member;

    let memberInDB = await repo.getMember({domain});
    if (memberInDB) cachedMembers.push(memberInDB);
    return memberInDB;
}

const getAllMembers = () => {
    return cachedMembers;
}

const updateMember = async (member) => {
    let savedMember = await repo.updateMember(member);
    let cacheIndex = cachedMembers.findIndex(cachedMember => cachedMember.domain == member.domain);
    if (cacheIndex >= 0) {
        cachedMembers[cacheIndex] = savedMember;
    }
    else {
        cachedMembers.push(savedMember);
    }

    return savedMember;
}

module.exports = {
    getMemberByDomain,
    getMemberByUsername,
    getAllMembers,
    updateMember
}