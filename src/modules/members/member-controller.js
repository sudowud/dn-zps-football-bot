const { formatName } = require("./member-util");
const memberService = require("./member-cached-repository");

const commandGetAllMember = async (content) => {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	splited.shift();

	let members = await memberService.getAllMembers();
	let response = [];
	for (let member of members) {
		if (splited.length == 0) {
			response.push(member);
			continue;
		}

		let memberInfo = {}
		for (let key of splited) {
			memberInfo[key] = member[key];
		}
		response.push(memberInfo);
	}
	return JSON.stringify(
		response
	)
}

const commandRegisterMember = async (content) => {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	if (splited.length != 3) {
		return "Cú pháp lỗi. Kiểm tra theo mẫu help.";
	}
	let domain = formatName(splited[1]);
	let username = formatName(splited[2]);

	let member = await memberService.updateMember({domain, username});
	return `Domain "${member.domain}" đã được cập nhật cho username "${member.username}" `
}


const commandSetRole = async(content) => {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	if (splited.length != 3) {
		return "Cú pháp lỗi. Kiểm tra theo mẫu help.";
	}
	let username = formatName(splited[1]);
	let role = splited[2];
	let member = await memberService.getMemberByUsername(username);

	if (!member) {
		return `Username ${username} chưa được đăng ký`;
	}

	member.role = role;
	await memberService.updateMember(member);
	return `Update role ${username} thành ${role} thành công!!!`
}

const commandSetMemberStatus = async(content) => {
	content = content.replace(/\s+/g, ' ').trim();
	let splited = content.split(" ");
	if (splited.length != 3) {
		return "Cú pháp lỗi. Kiểm tra theo mẫu help.";
	}
	let username = formatName(splited[1]);
	let status = splited[2].toUpperCase();
	let member = await memberService.getMemberByUsername(username);

	if (!member) {
		return `Username ${username} chưa được đăng ký`;
	}

	member.status = status;
	await memberService.updateMember(member);
	return `Update status ${username} thành ${status} thành công!!!`
}

module.exports = {
	commandRegisterMember,
    commandSetRole,
	commandSetMemberStatus,
	commandGetAllMember
}