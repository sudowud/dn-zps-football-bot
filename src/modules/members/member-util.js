const formatName = (username)  => {
	if (!username) return "";
	username = username.replace(/\@/g, "");
	return username.toLowerCase();
}

module.exports = {
    formatName
}