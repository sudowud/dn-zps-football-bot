const {setGroupIdOfFootball, getGroupIdOfFootBall, getGroupIdOfNotification, setGroupIdOfNotification} = require("./misc-repository");
const commandSetGroupFootball = async (chatId) => {
    await setGroupIdOfFootball(chatId);
    let checkGroupId = await getGroupIdOfFootBall();
    if (checkGroupId == chatId) {
        return "Update group football thành công : " + chatId;
    }
    return "Update group football thất bại: " + chatId;
}

const commandSetGroupNotification = async (chatId) => {
    await setGroupIdOfNotification(chatId);
    let checkGroupId = await getGroupIdOfNotification();
    if (checkGroupId == chatId) {
        return "Update group football thành công : " + chatId;
    }
    return "Update group football thất bại: " + chatId;
}

module.exports = {
    commandSetGroupFootball,
    commandSetGroupNotification
}