const mongoose = require('mongoose')
const Misc = new mongoose.Schema({
    _immutable: {
        type: Boolean,
        default: true,
        required: true,
        unique : true,
        immutable: true,
    },
    groupIdOfFootball: Number,
    groupIdOfNotification: Number
})

module.exports = mongoose.model('Misc', Misc);