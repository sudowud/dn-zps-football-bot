const Misc = require("./misc-model")
const {logger} = require("../../services/log-service");

const _createIfNotExist = async () => {
    let misc = await Misc.findOne({});
    if (!misc) {
        await Misc.create({});
        logger.info("Không có misc trong DB nên đã init {}")
    }
    logger.info("Đã tồn tại misc trong DB");
}

_createIfNotExist();

async function setGroupIdOfFootball(chatId) {
    return Misc.updateOne({}, {groupIdOfFootball: chatId});
}

async function getGroupIdOfFootBall() {
    let query = await Misc.findOne({}, "groupIdOfFootball").exec();
    return query.groupIdOfFootball;
}

const setGroupIdOfNotification = async(chatId) => {
    return Misc.updateOne({}, {groupIdOfNotification: chatId});
}

const getGroupIdOfNotification = async() => {
    let query = await Misc.findOne({}, "groupIdOfNotification").exec();
    return query.groupIdOfNotification;
}


module.exports = {
    setGroupIdOfFootball,
    getGroupIdOfFootBall,
    setGroupIdOfNotification,
    getGroupIdOfNotification
}