const schedule = require('node-schedule');
const { logger } = require('./log-service');
const { processNewNextMatchByDefault, processUnpinCurrentDayMatches, processRemindVote, processStopPoll } = require("../modules/matches/match-service");
const { DAY_IN_WEEK } = require("../common-util/time-util");
const { getMatchId, getNextMatch } = require("../modules/matches/match-util");

async function scheduleNewMatchPoll() {
	const rule = new schedule.RecurrenceRule();
	rule.dayOfWeek = [DAY_IN_WEEK.THURSDAY];
	rule.hour = 20;
	rule.minute = 0;
	rule.second = 0;
	const job = schedule.scheduleJob(rule, async function () {
		logger.info("onScheduleNewMatchPoll");
		await processUnpinCurrentDayMatches();

		await processNewNextMatchByDefault();
	});
}

async function scheduleRemindPoll() {
	const rule = new schedule.RecurrenceRule();
	rule.dayOfWeek = [DAY_IN_WEEK.WEDNESDAY];
	rule.hour = [7, 11];
	rule.minute = 0;
	rule.second = 0;
	const job = schedule.scheduleJob(rule, async function () {
		let matchId = getMatchId(getNextMatch(new Date()));
		logger.info("scheduleRemindPoll", matchId);
		let res = await processRemindVote(matchId);
		logger.info("scheduleRemindPoll result", res);
	});
}

async function scheduleStopMatchPoll() {
	const rule = new schedule.RecurrenceRule();
	rule.dayOfWeek = [DAY_IN_WEEK.WEDNESDAY];
	rule.hour = 12;
	rule.minute = 0;
	rule.second = 0;
	const job = schedule.scheduleJob(rule, async function () {
		let matchId = getMatchId(getNextMatch(new Date()));
		logger.info("onScheduleStopMatchPoll", matchId);
		let res = processStopPoll(matchId);
		logger.info("scheduleStopMatchPoll result", res);
	})
};


function runSchedule() {
	scheduleNewMatchPoll();
	scheduleStopMatchPoll();
	scheduleRemindPoll();
}

module.exports = {
	runSchedule
}
