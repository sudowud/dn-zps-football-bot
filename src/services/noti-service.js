const {getGroupIdOfNotification, getGroupIdOfFootBall} = require("../modules/misc/misc-repository");
const {bot} = require("../common/bot-tele");

let groupFootball = getGroupIdOfFootBall()
const sendToGroupFootball = async (message) => {
    if (!groupFootball) {
        groupFootball = await getGroupIdOfNotification();
    }
    if (groupFootball) {
        await bot.telegram.sendMessage(groupFootball, `[${process.env.MODE || "UNSET_MODE"}]: ` + message);
    }
}


let groupNotifications;
const sendToGroupNotification = async (message) => {
    if (!groupNotifications) {
        groupNotifications = await getGroupIdOfNotification();
    }
    if (groupNotifications) {
        await bot.telegram.sendMessage(groupNotifications, `[${process.env.MODE || "UNSET_MODE"}]: ` + message);
    }
}

module.exports = {
    sendToGroupFootball,
    sendToGroupNotification
}