require('dotenv').config();

const authorMiddle = require("../middleware/author-middleware");

const { logger } = require('./log-service');
const { formatBotCommand, COMMANDS, getHelps} = require('../common/Commands');
const { commandRegisterMember, commandSetRole, commandGetAllMember, commandSetMemberStatus} = require('../modules/members/member-controller');
const asyncWrapper = require('../middleware/async');
const { bot } = require('../common/bot-tele');
const {commandSetGroupFootball, commandSetGroupNotification} = require("../modules/misc/misc-controller");
const {addMatchHandler} = require("../modules/matches/match-bot");

function startBot() {
	bot.use(authorMiddle.determineRole);

	bot.start(authorMiddle.isAdmin, (ctx) => {
		try {
			let message = `Football Bot has been started!`;
			ctx.reply(message)
		} catch (error) {

		}
	});

	bot.help((ctx) => {
		try {
			let message = getHelps();
			ctx.reply(message)
		} catch (error) {

		}
	});


	bot.command(COMMANDS.set_group_football, authorMiddle.isSuperAdmin, asyncWrapper(async (ctx) => {
		let response = await commandSetGroupFootball(ctx.chat.id);
		ctx.reply(response);
		logger.info(`[${COMMANDS.set_group_football}]`, ", sender:", ctx.from?.username, ", chat id: ", ctx.chat.id, ", response: ", response);
	}));

	bot.command(COMMANDS.set_group_notification, authorMiddle.isSuperAdmin, asyncWrapper(async (ctx) => {
		let response = await commandSetGroupNotification(ctx.chat.id);
		ctx.reply(response);
		logger.info(`[${COMMANDS.set_group_notification}]`, ", sender:", ctx.from?.username, ", chat id: ", ctx.chat.id, ", response: ", response);
	}));

	bot.command(COMMANDS.remove_group_notification, authorMiddle.isSuperAdmin, asyncWrapper(async (ctx) => {
		let response = await commandSetGroupNotification(0);
		ctx.reply(response);
		logger.info(`[${COMMANDS.remove_group_notification}]`, ", sender:", ctx.from?.username, ", chat id: ", ctx.chat.id, ", response: ", response);
	}));

	bot.command(COMMANDS.get_all_member, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
		let response = await commandGetAllMember(ctx.message.text);
		ctx.reply(response);
	}))

	bot.command(COMMANDS.set_role, authorMiddle.isSuperAdmin, asyncWrapper(async (ctx) => {
		let response = await commandSetRole(ctx.message.text);
		ctx.reply(response);
		logger.info(`[${COMMANDS.set_role}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
	}));

	bot.command(COMMANDS.register_member, authorMiddle.isSuperAdmin, asyncWrapper(async (ctx) => {
		let response = await commandRegisterMember(ctx.message.text);
		ctx.reply(response);
		logger.info(`[${COMMANDS.register_member}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
	}));

	bot.command(COMMANDS.set_member_status, authorMiddle.isAdmin, asyncWrapper(async (ctx) => {
		let response = await commandSetMemberStatus(ctx.message.text);
		ctx.reply(response);
		logger.info(`[${COMMANDS.set_member_status}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
	}));

	bot.command(COMMANDS.get_all_member, authorMiddle.isSuperAdmin, asyncWrapper(async (ctx) => {
		let response = await commandSetRole(ctx.message.text);
		ctx.reply(response);
		logger.info(`[${COMMANDS.set_role}]`, ", sender:", ctx.from?.username, ", text: ", ctx.message.text, ", response: ", response);
	}));
	addMatchHandler();

	bot.launch()

	bot.telegram.setMyCommands(formatBotCommand());
}


module.exports = {
	startBot
}